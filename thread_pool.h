#include <condition_variable>
#include <queue>
#include <future>
#include <mutex>
#include <vector>
#include <thread>

struct VoidFunction
{
    std::function<void(void)> function_call_;
};

struct StringArgFunction
{
    std::function<void(std::string)> function_call_;
    std::string argument_;
};

class ThreadPool 
{
 public:
    ThreadPool(int num_threads = 1) : num_threads_(num_threads) 
    {
         for (int idx = 0; idx < num_threads_; ++idx) {
            workers_.emplace_back([this]
            {
                while (true) {
                    //Note: Lock a mutex, so no 2 workers try accessing the same task within the tasks queue
                    std::unique_lock<std::mutex> lock(mux_);
                    //Note: set up the conditonal variable on when to allow a worker access the task queue
                    condition_variable_.wait(lock, [this](){ return is_stopped_ || !tasks_.empty(); });
                    //Note: Add base case when we exit the threading
                    if (is_stopped_ && tasks_.empty()) { return; }
                    condition_variable_.notify_one();
                    //Note: Pull from the tasks queue and pop it afterwards (lock is set on the front element of the tasks queue)
                    auto task = std::move(tasks_.front());
                    tasks_.pop();
                    lock.unlock();

                    task();
                }
            });
        }
    }

    ~ThreadPool() 
    {
        {
            std::unique_lock<std::mutex> lock(mux_);
            is_stopped_ = true;
        }
        
        //Note: Wake threads, so they see the condition
        condition_variable_.notify_all();
        for (auto& th : workers_) { th.join(); }
    } 

    template <typename Function, class ...Args>
    void AddTaskToPool(Function&& task, Args&&... fargs)
    {
        //Note: std::packaged_task is just like std::function but async
        using return_type = typename std::result_of<Function(Args ...)>::type;
        auto smart_ptr_wrapper = std::make_shared<std::packaged_task<return_type()>>(std::bind(std::forward<Function>(task), std::forward<Args>(fargs) ...));
        //TODO: std::future<task_return_type> return_value = packaged_task.get_future();
        //Note: Common variables to threads have to be protected
        {
            std::unique_lock<std::mutex> lock(mux_);

            if (is_stopped_) { throw std::runtime_error("Pool has been stopped!\n"); }

            tasks_.emplace([smart_ptr_wrapper]() { (*smart_ptr_wrapper)(); }); 
        }

        condition_variable_.notify_one();
    }

    void Stop() { is_stopped_ = false; }

 private:  
    bool is_stopped_ = false;    
    int num_threads_;

    std::condition_variable condition_variable_;
    std::mutex mux_;

    std::vector<std::thread> workers_;
    std::queue<std::function<void()>> tasks_;
};
