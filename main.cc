#include <chrono>
#include <iostream>
#include <fstream>
#include <limits>
#include <memory>
#include <vector>

#include "thread_pool.h"

#define NUM_JOBS 100

template <typename T_>
void long_compute() {
    auto max_value = std::numeric_limits<T_>::max() / 2;
    for (auto idx = 0; idx < 100; ++idx){
        std::cout << "\rtest...";
    }
}

void write2file(int idx)
{
    std::string msg("hello");
    std::fstream fs(std::string("test_data/test_file_") + std::to_string(idx) + std::string(".txt"), std::ios::out);
    if (fs.is_open())
    {
        fs.write(msg.c_str(), static_cast<int>(msg.size()));
        fs.close();
    }
}

int main(void) {
    std::unique_ptr<ThreadPool> pool = std::make_unique<ThreadPool>(20);
    
    std::chrono::high_resolution_clock::time_point start = std::chrono::high_resolution_clock::now();

    std::vector<std::function<void(void)>> tasks;
    for (int idx = 0; idx < static_cast<size_t>(NUM_JOBS); ++idx) { pool->AddTaskToPool(&write2file, idx); }
    pool->Stop();
    
    std::chrono::high_resolution_clock::time_point end = std::chrono::high_resolution_clock::now();
    auto diff_threaded = std::chrono::duration_cast<std::chrono::nanoseconds>(end - start).count();
    std::cout << "\nthreaded time(ns)= " << diff_threaded << "\n";

    std::cout << "next time is \n\n";

    start = std::chrono::high_resolution_clock::now();

    for (int idx = 0; idx < static_cast<size_t>(NUM_JOBS); ++idx) {
        write2file(100000);
    }

    
    end = std::chrono::high_resolution_clock::now();
    auto diff = std::chrono::duration_cast<std::chrono::nanoseconds>(end - start).count();
    std::cout << "\ntime(ns)= " << diff << "\n";

    return 0;
}

