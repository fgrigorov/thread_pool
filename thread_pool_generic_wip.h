#include <condition_variable>
#include <deque>
#include <future>
#include <mutex>
#include <vector>
#include <thread>
#include <tuple>

template <typename T_>
struct Type {
    T_ type_;
};

template <typename T_>
bool IsSelfEmpty() { return std::is_empty<T_>::value; }  

template <>
struct FunctionSignatureType {
    bool is_empty_ = IsSelfEmpty<decltype(*this)>();
};

template <typename ReturnType, class ...Args>
struct FunctionSignatureType {
    bool is_empty_ = IsSelfEmpty<delctype(*this)>();
    bool is_void_ = std::is_void<ReturnType>::value;

    std::tuple<Args> arg_types_ = std::make_tuple<Args>(std::forward<Args>(Args ...));
    ReturnType type_;
};

template <typename ReturnType, class ...Args>
struct FunctionStack
{
 public:
    using stack = std::pair<std::function<ReturnType(Args ...)>, std::tuple<Args>>;
    stack operator()(Args ...fargs) {
        size_args_ = std::make_index_sequence<sizeof ...(Args)>;
        stack_ = std::move(std::make_pair(func, std::make_tuple(&fargs...)));
    }

    std::integer_sequence<sizeof ...(Args)> size_args_;
    stack stack_;
};

class ThreadPool 
{
 public:
    ThreadPool(int num_threads = 1) : num_threads_(num_threads) {}

    ~ThreadPool() {
        {
            std::unique_lock<std::mutex> lock(mux_);
            is_stopped_ = true;
        }
        
        //Note: Wake threads, so they see the condition
        condition_variable_.notify_all();
        for (auto& th : workers_) { th.join(); }
    }

    //@ Initiate the thread pool (run the threads, so they can be alive)
    void InitiateThreadPool() {
        for (int idx = 0; idx < num_threads_; ++idx) {
            workers_.emplace_back([this]
            {
                while (true) {
                    //Note: Lock a mutex, so no 2 workers try accessing the same task within the tasks queue
                    std::unique_lock<std::mutex> lock(mux_);
                    //Note: set up the conditonal variable on when to allow a worker access the task queue
                    condition_variable_.wait(lock, [this](){ return is_stopped_ || !tasks_.empty(); });
                    //Note: Add base case when we exit the threading
                    if (is_stopped_ && tasks_.empty()) { return; }
                    condition_variable_.notify_one();
                    //Note: Pull from the tasks queue and pop it afterwards (lock is set on the front element of the tasks queue)
                    auto task = std::move(tasks_.front());
                    tasks_.pop_front();
                    lock.unlock();

                    //Note: Unpack the indices to access the tuple's values at compile time
                    task.stack_.first(std::get<task.size_args_>(task.stack_.second) ...);//TODO
                }
            });
        }        
    }

    //@ Register what the function would look like: ReturnType Function(Args1, Arg2, ..., Argn)
    template <typename ReturnType, class ...Args>
    void RegisterFunctionSignature() {
        task_signature_ = FunctionSignatureType<ReturnType, Args...>();   
    }

    template <typename Function, class ...Args>
    void add(const Function& task, Args&&... fargs)
    {
        //Note: std::packaged_task is just like std::function but async
        //std::future<task_return_type> return_value = packaged_task.get_future();
        //Note: Common variables to threads have to be protected
        {
            std::unique_lock<std::mutex> lock(mux_);

            if (is_stopped_) { throw std::runtime_error("Pool has been stopped!\n"); }

            FunctionStack<ReturnType, Args> task;
            task(&frags ...);
            tasks_.push_back(task); 
        }

        condition_variable_.notify_one();
    }

    void Stop() { is_stopped_ = false; }

 private:  
    bool is_stopped_ = false;    
    int num_threads_;

    template <typename ReturnType, class ...Args>
    using signature = FunctionSignatureType<ReturnType, Args...>;
    signature task_signature_;

    std::condition_variable condition_variable_;
    std::mutex mux_;

    std::vector<std::thread> workers_;

    template <typename ReturnType, class ...Args>
    std::deque<FunctionStack<ReturnType, Args>> tasks_;
};
